import numpy as np
import pandas as pd
from collections import defaultdict
import helpers.preprocessing

import tensorflow as tf
import tensorflow_gnn as tfgnn


class MakeTensors():

    def __init__(self, articles: pd.DataFrame, users: pd.DataFrame) -> tfgnn.graph.graph_tensor.GraphTensor:
        self.articles = articles
        self.users = users

    def build_article_nodes(self):

        self.article_nodes = articles['article_id'] # len(articles_series) == count(articles)

    def build_article_embeddings(self):

        ## one hot encode the subcategory for each article - our node features
        dummies = pd.get_dummies(articles['subcategory'])
        self.article_embeddings = dummies.to_numpy().astype(int)
        assert len(self.articles_series) == self.article_embeddings.shape[1] # dummy variables for every article

    def build_user_nodes(self):

        self.user_nodes = users['user_id'].unique()

    def build_user_embeddings(self):

        self.user_embeddings = None

    def build_edges(self):

        # what's the handiest way to store this info? defaultdict(defaultdict(int)). We want clicks[userid][article_id] = num_clicks
        clicks = collections.defaultdict(lambda: collections.defaultdict(int))
        noclicks = collections.defaultdict(lambda: collections.defaultdict(int))
        user_mapper = dict(zip(self.user_nodes, range(0, len(self.user_nodes))))

        ## building adjacency lists
        ### let's start by working out what the sources and target edges are, and what the weight is.
        for ind, row in train[['user_id', 'click', 'no_clicks']].iterrows():

            user_int = user_mapper[row['user_id']]

            for article in row['click']:

                article_int = articles_mapper[article]
                clicks[user_int][article_int] += 1

            for article in row['no_clicks']:

                article_int = articles_mapper[article]
                noclicks[user_int][article_int] += 1

        ### now we build three arrays for each edge set, each with length equal to the number of edges
        click_user, click_article, click_weight, noclick_user, noclick_article, noclick_weight = [], [], [], [], [], []

        for user in clicks:
            for article in clicks[user]:
                click_user.append(user)
                click_article.append(article)
                click_weight.append([clicks[user][article]])

        for user in noclicks:
            for article in noclicks[user]:
                noclick_user.append(user)
                noclick_article.append(article)
                noclick_weight.append([noclicks[user][article]])

        self.edges = {}
        self.edges['click']['user'] = click_user
        self.edges['click']['article'] = click_article
        self.edges['click']['weight'] = click_weight

        self.edges['noclick']['user'] = noclick_user
        self.edges['noclick']['article'] = noclick_article
        self.edges['noclick']['weight'] = noclick_weight

    def build_graph(self):

        graph = tfgnn.GraphTensor.from_pieces(
           node_sets={
               "user": tfgnn.NodeSet.from_fields(
                   sizes=tf.shape(self.user_nodes),
                   features={}),

               "article": tfgnn.NodeSet.from_fields(
                   sizes=tf.shape(self.article_nodes),
                   features={
                       "embedding": tf.constant(self.article_embeddings),
                   })},

           edge_sets={
               "click": tfgnn.EdgeSet.from_fields(
                   features = {'num_clicks': self.edges['click']['weight']},
                   sizes=tf.shape(self.edges['click']['user']),
                   adjacency = tfgnn.Adjacency.from_indices(('user', self.edges['click']['user']), ('article', self.edges['click']['article']))),

               "noclick": tfgnn.EdgeSet.from_fields(
                   features = {'num_noclicks': self.edges['noclick']['weight']},
                   sizes=tf.shape(self.edges['noclick']['user']),
                   adjacency = tfgnn.Adjacency.from_indices(('user', self.edges['noclick']['user']), ('article', self.edges['noclick']['article'])))
           }
        )

        schema = tfgnn.read_schema('schema.pbtxt')
        graph_spec = tfgnn.create_graph_spec_from_schema_pb(schema)
        assert graph_spec.is_compatible_with(graph), 'Graph spec does not match graph structure'

        self.graph = graph
        return self.graph
