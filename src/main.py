import numpy as np
import pandas as pd
from collections import defaultdict
import helpers.preprocessing

import tensorflow as tf
import tensorflow_gnn as tfgnn

                                                            ### Imports & Preprocessing ###

# articles
articles = pd.read_csv('../../datasets/MIND_news_recommendation/news.tsv/news.tsv',sep='\t', header=None)
articles.rename(columns = {0: 'article_id', 1: 'category', 2: 'subcategory', 3: 'title', 4: 'abstract', 5: 'url',
                           6: 'title_entities', 7: 'abstract_entities'}, inplace = True)
articles_mapper = dict(zip(articles['article_id'], articles.index))
articles_series = articles['article_id']

## one hot encode the subcategory for each article - our node features
dummies = pd.get_dummies(articles['subcategory'])
article_features = dummies.to_numpy().astype(int)
assert articles.shape[0] == dummies.shape[0] # check we have an embedding for every feature

# behaviours
behaviours = pd.read_csv('../../datasets/MIND_news_recommendation/MINDsmall_train/behaviors.tsv',sep='\t', header=None)
behaviours.rename(columns = {0: 'impression_id', 1: 'user_id', 2: 'impression_time', 3: 'click_history', 4: 'impressions'}, inplace = True)
behaviours.set_index('impression_id', inplace = True)
behaviours.drop('click_history', axis = 1, inplace = True)
behaviours['impression_time'] = pd.to_datetime(behaviours['impression_time'])

behaviours['click'], behaviours['no_clicks'] = zip(*behaviours['impressions'].map(preprocessing.process_impressions))
behaviours.drop('impressions', axis = 1, inplace = True)

print(f'behaviours has {behaviours.shape[0]:,} rows')
print(f'there are {behaviours.user_id.nunique():,} unique user_ids')

# now we need to split out into train and test sets. Let's take the last 10% of the impressions to use as a test set
behaviours.sort_values(by = 'impression_time', ascending = True, inplace = True)
train = behaviours[:-15_000]
test = behaviours[-15_000:]

print(f'test df is {round(100 * (test.shape[0] / behaviours.shape[0]), 2)}% the size of behaviours df')
assert train.impression_time.max() < test.impression_time.min() # check our time series split worked correctly

# edges
users_series = train['user_id'].unique()
user_mapper = dict(zip(train['user_id'].unique(), range(0, len(train['user_id'].unique()))))

# what's the handiest way to store this info? defaultdict(defaultdict(int)). We want clicks[userid][article_id] = num_clicks
clicks = collections.defaultdict(lambda: collections.defaultdict(int))
noclicks = collections.defaultdict(lambda: collections.defaultdict(int))

## building adjacency lists
### let's start by working out what the sources and target edges are, and what the weight is.
for ind, row in train[['user_id', 'click', 'no_clicks']].iterrows():

    user_int = user_mapper[row['user_id']]

    for article in row['click']:

        article_int = articles_mapper[article]
        clicks[user_int][article_int] += 1

    for article in row['no_clicks']:

        article_int = articles_mapper[article]
        noclicks[user_int][article_int] += 1

### now we build three arrays for each edge set, each with length equal to the number of edges
click_user, click_article, click_weight, noclick_user, noclick_article, noclick_weight = [], [], [], [], [], []

for user in clicks:
    for article in clicks[user]:
        click_user.append(user)
        click_article.append(article)
        click_weight.append([clicks[user][article]])

for user in noclicks:
    for article in noclicks[user]:
        noclick_user.append(user)
        noclick_article.append(article)
        noclick_weight.append([noclicks[user][article]])

# build graph
graph = tfgnn.GraphTensor.from_pieces(
   node_sets={
       "user": tfgnn.NodeSet.from_fields(
           sizes=tf.shape(users_series),
           features={}),

       "article": tfgnn.NodeSet.from_fields(
           sizes=tf.shape(articles_series),
           features={
               "embedding": tf.constant(article_features),
           })},

   edge_sets={
       "click": tfgnn.EdgeSet.from_fields(
           features = {'num_clicks': click_weight},
           sizes=tf.shape(click_user),
           adjacency = tfgnn.Adjacency.from_indices(('user', click_user), ('article', click_article))),

       "noclick": tfgnn.EdgeSet.from_fields(
           features = {'num_noclicks': noclick_weight},
           sizes=tf.shape(noclick_user),
           adjacency = tfgnn.Adjacency.from_indices(('user', noclick_user), ('article', noclick_article)))
   }
)

# check graph matches our expected schema
schema = tfgnn.read_schema('schema.pbtxt')
graph_spec = tfgnn.create_graph_spec_from_schema_pb(schema)
assert graph_spec.is_compatible_with(graph)

# export
with tf.io.TFRecordWriter('data/graph.tfrecord') as writer:
    example = tfgnn.write_example(graph)
    writer.write(example.SerializeToString())
