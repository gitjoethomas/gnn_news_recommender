# News page recommender with GNN

## Task
Given:
* Embeddings to represent news articles
* Users visitation history  

Produce a ranked list of articles that the users should visit via a recommender system.

## Rationale
This is a good opportunity to leverage a graph neural network. We can treat each news article as a node and use users' past visitation history to make edges between said nodes. The graph will be directed because visitation is directional.

If visitation forms the edges between the nodes, then the message passing round in the GNN will make node embedding A similar to B to one another if a user frequently visits node A from B.

When we're deciding which pages to recommend to users, we want to recommend pages that other users typically have visited from the pages that this specific user has visited recently. That means we should recommend the pages that have embeddings most similar to the embeddings of the recently visited pages.

## Evaluation

### Data
We want to predict what pages users _will_ visit so we can't pick a random % of our data to test on. We'll take the last 10% of visitation history and try to predict it. This isn't a time-series, we're just saying "users will visit these over the next n days".

### Metrics
* Mean average precision & recall @k: [here's](http://sdsawtelle.github.io/blog/output/mean-average-precision-MAP-for-recommender-systems.html) a good article on MAP & MAR.
    * tl;dr: it's the typical precision calculation but getting true positives earlier in your recommended list will increase precision.
    * What value should K be? We'll decide this later but people typically don't read many news articles back to back so it will be low (<5)
* Coverage: What % of the items in the training set are recommended in the test set? A low score would be signal that we might be overengineering the solution, and should perhaps just accept the baseline model.
* Personalization: How distinct are the recommendations being given to people? Higher score means more distinct. Works by calculating cosine (dis)similarity across recommendations.

## Approach

### Baseline model
We'll use a simple rules-based approach as a baseline model to get a sense of how much incremental value the GNN is bringing. The process will recommend random articles to users. If we were in a production system I would have this instead recommend articles from the front page (which is presumably maintained manually).

### GNN options

#### Heterogenous GNN
1. Build graph: two types of nodes: users and articles, and two types of edges: click and no click. We'll draw undirected edges between an article and user when either a click or no click occurs.
2. Update node embeddings via 2 hop message passing. Embeddings should become more similar if they follow a click edge, and less similar if a no click.
3. Link prediction: predict edges between a user and all unread articles within 3 hops of them.

#### GNN & euclidian distance of embeddings
1. Build graph: draw an edge from node A to node B if a user visited B within _n_ minutes of node A. We'll experiment with different values for _n_. We'll weight the edges by the number of visits.
2. Update node embeddings via _i_ hop message passing.  We'll also experiment with different values for _i_. Now node embeddings A will be more similar to B 1) article A is already similar to article B, 2) if users frequently visit article A within _n_ minutes of visiting article B.
3. From (2) we know which articles are most similar to any single article. But we need to take more into account than just the user's most recently viewed article. So we need to aggregate together the embeddings for the _j_ articles most recently viewed by the user to produce a 'user context'.
4. Recommend articles: identify the _k_ articles to be shown to the user. This is done by taking the _k_ articles with the smallest euclidian distance between their article embedding updated in (2), and the user context generated in (3).

### Preprocessing
1. Remove popular items?

## Results
tbd
