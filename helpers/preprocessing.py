def process_impressions(impressions: str) -> [str, str]:

    clicks, noclicks = [], []
    
    for impression in impressions.split(): # iterate through impressions and append to relevant list depending on whether behaviour was a click or not
        if impression[-1] == '1':
            clicks.append(impression[:-2])
        elif impression[-1] == '0':
            noclicks.append(impression[:-2])
            
    return clicks, noclicks